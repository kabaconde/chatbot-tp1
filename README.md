**Install followings**

- node and npm : https://nodejs.org/en/download/package-manager/

- vs code editor

- vs code extension : TSLINT, DOTEnv,  vs code icons


**Download : bot emulator**

- from https://github.com/Microsoft/BotFramework-Emulator/releases

- pick up this file : botframework-emulator-3.5.35-x86_64.AppImage


**In current directory**

- Run : npm init

- Run : npm i botbuilder -P

- Run : npm i restify -P

- Run : npm i dotenv -P

- Run : npm i spacex-api-wrapper --save

- Run : npm i adaptivecards --save