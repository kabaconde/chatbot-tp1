require('dotenv').config();
var restify = require("restify");
var builder = require('botbuilder');
var inMemoryStorage = new builder.MemoryBotStorage();
var request = require('request');

// SpaceX
const SpaceXAPI = require('spacex-api-wrapper');
// const SpaceXAPI = require('SpaceX-API-Wrapper');
let SpaceX = new SpaceXAPI();

// Server
var server = restify.createServer();
server.listen(process.env.PORT, function () {
    console.log("%s listening to %s", server.name, server.url);
})

// Connector
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});
server.post('/api/messages', connector.listen());

//Universal bot
var bot = new builder.UniversalBot(connector, [
    function (session) {
        session.send("Bienvenue sur SpaceX");
        // session.beginDialog('greetings', session.userData.profile);
        session.beginDialog('menu', session.userData.profile);
    },
    // function(session, results){
    //     if(!session.userData.profile){
    //         session.userData.profile = results.response;
    //     }
    //     session.send(`Hello ${session.userData.profile.name} ;)`);
    // }
]).set('storage', inMemoryStorage);

//dialogues

bot.dialog('greetings', [
    // step 1
    function (session, results, next) {
        session.dialogData.profile = results || {};
        if (!session.dialogData.profile.name) {
            builder.Prompts.text(session, 'what is your name ?');
        } else {
            next();
        }
    },
    // step 2
    function (session, results) {
        if (results.response) {
            session.dialogData.profile.name = results.response;
        }
        session.endDialogWithResult({
            response: session.dialogData.profile
        });
    }
]);

var menuItems = {
    "Connaitre les prochains lancement": {
        item: 'choice1'
    },
    "Avoir des informations sur les capsules": {
        item: 'choice2'
    },
    "Avoir les informations sur les rockets": {
        item: 'choice3'
    },
    "Avoir les informations sur les lauchpads": {
        item: 'choice4'
    },

}

bot.dialog('menu', [
    function (session) {
        builder.Prompts.choice(session, 'En quoi puis-je vous aider ?', menuItems, {
            listStyle: 3
        })
    },
    function (session, results) {
        var choice = results.response.entity;
        var item = menuItems[choice].item;
        session.beginDialog(item);
    }
]);

bot.dialog('choice1', [
    function (session) {
        request("https://api.spacexdata.com/v2/info", function (error, response, body) {
            var adaptativeCard = {
                "type": "AdaptativeCard",
                "text": response.name,
                "attachments": [{
                    "contentType": "application/vnd.microsoft.card.adaptive",
                    "content": {
                        "type": "AdaptiveCard",
                        "version": "1.0",
                        "body": [{
                                "type": "TextBlock",
                                "text": response.summary,
                                "size": "large"
                            },
                            {
                                "type": "TextBlock",
                                "text": "*Sincerely yours,*"
                            },
                            {
                                "type": "TextBlock",
                                "text": "Adaptive Cards",
                                "separation": "none"
                            }
                        ],
                        "actions": [{
                            "type": "Action.OpenUrl",
                            "url": "http://spacex.com/",
                            "title": "Learn More"
                        }]
                    }
                }]
            }
            //session.endDialogWithResult(adaptativeCard);
            session.send(JSON.stringify(response));
            //session.send(adaptativeCard);
        });

    }
]);

bot.dialog('choice2', [
    function (session, results) {
        request('https://api.spacexdata.com/v2/capsules', function (error, response, body) {
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            //console.log('body:', body); // Print the HTML the url
            session.send(JSON.stringify(response));
        });
    }
]);

bot.dialog('choice3', [
    function (session) {
        request('https://api.spacexdata.com/v2/rockets', function (error, response, body) {
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            //console.log('body:', body); // Print the HTML the url
            session.send(JSON.stringify(response));
        });
    }
]);

bot.dialog('choice4', [
    function (session) {
        request('https://api.spacexdata.com/v2/launchpads', function (error, response, body) {
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            //console.log('body:', body); // Print the HTML the url
            session.send(JSON.stringify(response));
        });
    }
]);